package e.jdlopez.musicpi;

import java.net.Socket;

/**
 * This class holds a single instance of the socket connection for the app to use
 *
 * @author Jose Lopez
 */

class SocketHandler {
    private static Socket socket;

    static synchronized Socket getSocket() {
        return socket;
    }

    static synchronized void setSocket(Socket socket) {
        SocketHandler.socket = socket;
    }
}
