package e.jdlopez.musicpi;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Switch;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This class is the visual part of the app, the GUI
 *
 * @author Jose Lopez
 */

public class previewActivity extends AppCompatActivity {
    Spinner spinner, spinner2;
    Switch powerSwitch, muteSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        populateSpinner();

        powerSwitch = findViewById(R.id.power_switch);
        muteSwitch = findViewById(R.id.mute_switch);

        /*
         * Turns zone 1 on or off
         */
        powerSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String request = "POWER 1 ON";
                if (powerSwitch.isChecked()) {
                    request = "POWER 1 OFF";
                }

                String response = "";

                try {
                    BufferedOutputStream outToServer = new BufferedOutputStream(SocketHandler.getSocket().getOutputStream());
                    BufferedReader inFromServer = new BufferedReader(new InputStreamReader(SocketHandler.getSocket().getInputStream()), 1024);

                    Log.i("Connection", SocketHandler.getSocket().isConnected() + "");
                    Log.i("Request", request);

                    outToServer.write(request.getBytes());
                    outToServer.flush();

                    response += inFromServer.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Log.i("From Server", response);

            }
        });

    }

    /**
     * Populates the part mode input channel and set input channel
     */
    public void populateSpinner() {
        spinner = findViewById(R.id.part_mode_input_channel_spinner);
        spinner2 = findViewById(R.id.set_input_channel);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.Part_Mode_Input_Channel, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.Set_Input_Channel, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner2.setAdapter(adapter2);
    }
}
