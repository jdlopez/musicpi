package e.jdlopez.musicpi;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * This is the initial activity that the user sees.
 * This is just for debugging purposes, the idea is to remove this page and insert
 * the code for socket connection in previewActivity when the app is finished.
 *
 * @author Jose Lopez
 */

public class MainActivity extends AppCompatActivity {

    final Handler handler = new Handler();
    EditText serverIP, serverPort, requestInput;
    TextView connectionStatus, responseOutput;
    Button connectBtn, requestBtn, disconnectBtn, previewBtn;
    String response = "";

    /*
     * 8664
     * 173.22.75.109
     * 192.168.1.255
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        serverIP = findViewById(R.id.ip_address_input);
        serverPort = findViewById(R.id.port_input);
        requestInput = findViewById(R.id.request_input);
        responseOutput = findViewById(R.id.response);
        connectionStatus = findViewById(R.id.connection_status);
        connectBtn = findViewById(R.id.connect_btn);
        requestBtn = findViewById(R.id.request_btn);
        disconnectBtn = findViewById(R.id.disconnect_btn);
        previewBtn = findViewById(R.id.preview_btn);

        connectionStatus.setText("Not Connected");

        /*
         * Connect to the pi
         */
        connectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ipAddress = serverIP.getText().toString();
                int portNumber = Integer.parseInt(serverPort.getText().toString());
                handleSocket(ipAddress, portNumber);
            }
        });

        disconnectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disconnect();
            }//Disconnect onClick method
        });//DisconnectBtn onClick listener
    }

    public void handleSocket(final String ip, final int port) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    InetAddress serverAddr = InetAddress.getByName(ip);

                    SocketHandler.setSocket(new Socket(serverAddr, port));
                    Log.i("Connection", SocketHandler.getSocket().isConnected() + "");
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            connectionStatus.setText("Connected to: " + SocketHandler.getSocket().getInetAddress() + "\n" + SocketHandler.getSocket().isConnected());
                        }
                    });

                    if (SocketHandler.getSocket().isConnected()) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                requestInput.setVisibility(View.VISIBLE);
                                responseOutput.setVisibility(View.VISIBLE);
                                requestBtn.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                } catch (UnknownHostException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                /*
                 * Send commands to the pi
                 * Used for debugging purposes
                 */
                requestBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String request = requestInput.getText().toString();

                        if(request == null || request.equals(""))
                            return;

                        try {
                            BufferedOutputStream outToServer = new BufferedOutputStream(SocketHandler.getSocket().getOutputStream());
                            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(SocketHandler.getSocket().getInputStream()), 1024);
                            Log.i("Connection", SocketHandler.getSocket().isConnected() + "");
                            Log.i("Request", request);
                            outToServer.write(request.getBytes());
                            outToServer.flush();
                            response += inFromServer.readLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        Log.i("From Server", response);

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                responseOutput.setText("From Server: " + response);
                            }
                        });
                    }//OnClick Method
                });//Request onClick Listener
            }//End run()
        }).start();//End new Thread()
    }

    public void displayPreview(View view) {
        disconnect();
        Intent intent = new Intent(this, previewActivity.class);
        startActivity(intent);
    }

    /**
     * If socket is connected, disconnect
     */
    public void disconnect() {
        if (SocketHandler.getSocket() != null && SocketHandler.getSocket().isConnected()) {
            try {
                SocketHandler.getSocket().close();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        connectionStatus.setText("Not Connected");
                        requestInput.setVisibility(View.GONE);
                        responseOutput.setVisibility(View.GONE);
                        requestBtn.setVisibility(View.GONE);
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}